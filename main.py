import asyncio
import sqlite3

from Submodules.Defence.twitch_api import WoWnikCompany_client_id, WoWnikCompany_client_secret
from Submodules.TwitchHelixApiClient import TwitchHelixApiClient

twitch_api_client = TwitchHelixApiClient(client_id=WoWnikCompany_client_id, client_secret=WoWnikCompany_client_secret)

channel = "wownik"


def add_user(cur, name):
    cur.execute("INSERT INTO users (name) VALUES (?)", (name,))


def remove_user(cur, name):
    cur.execute("DELETE FROM users WHERE name = (?)", (name,))


async def get_followers():
    users = (await twitch_api_client.get_users(login=[channel])).data
    cursor = None
    follows = []

    while True:
        resp = await twitch_api_client.get_users_follows(after=cursor, first=100, to_id=users[0].id)
        follows.extend(resp.data)
        cursor = resp.pagination.get("cursor")
        if cursor is None:
            break
    return list(reversed([i.from_name for i in follows]))


async def main():
    con = sqlite3.connect("main.sqlite3")
    cur = con.cursor()

    cur.execute("SELECT name FROM users")

    db_followers = [row[0] for row in cur.fetchall()]
    twitch_followers = await get_followers()

    print("New followers:")
    for follower in twitch_followers:
        if not (follower in db_followers):
            print(follower)
            add_user(cur, follower)

    print()
    print("New unfollowers:")
    for follower in db_followers:
        if not (follower in twitch_followers):
            print(follower)
            remove_user(cur, follower)

    con.commit()

    input("\n\nPress button")


if __name__ == "__main__":
    loop = asyncio.get_event_loop()
    loop.run_until_complete(main())
